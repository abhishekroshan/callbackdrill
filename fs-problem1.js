const fs = require("fs");
//importing the fs module

function createAndDeleteFiles(directoryPath, numberOfFiles, callBack) {
  fs.mkdir(directoryPath, (error) => {
    if (error) {
      callBack(error);
      //if an error occurs during directory creation,call the callback with error
    } else {
      console.log("Directory created");

      for (let index = 1; index <= numberOfFiles; index++) {
        const fileName = `${directoryPath}/file${index}.json`;
        //create a file with at every iteration
        const randomData = { value: Math.random() };
        //create a object with random number

        const data = JSON.stringify(randomData);
        //writing json data to a file

        fs.writeFile(fileName, data, (error) => {
          //creating the file with data
          if (error) {
            callBack(error);
            //if error occurs we call the callback with error
          }
        });
      }

      fs.readdir(directoryPath, (error, files) => {
        //reading the directory
        if (error) {
          callBack(error);
          //if error occurs call the callback with error
        } else {
          files.forEach((fileName) => {
            //using for each to iterate through each item
            const filePath = `${directoryPath}/${fileName}`;
            //the file which we want to delete
            fs.unlink(filePath, (error) => {
              //deleting the file
              if (error) {
                callBack(error);
              }
            });
          });
          console.log("All files deleted");
        }
      });
    }
  });
}

module.exports = createAndDeleteFiles;
