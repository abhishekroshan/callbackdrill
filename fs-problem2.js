const fs = require("fs");

// Step 1: Read the given file lipsum.txt
function readingFile(filePath, callback) {
  //reading the file with readfile module
  fs.readFile(filePath, "utf8", (err, data) => {
    if (err) {
      return callback(err, null);
      //return callback if error occured
    }
    callback(null, data);
  });
}

// Step 2: Convert the content to uppercase & write to a new file
function convertToUppercase(data, callback) {
  const uppercasedContent = data.toUpperCase();
  //converting to uppercase
  const newFileName = "uppercased.txt";
  //the file where the upper cased data will be stored

  fs.writeFile(newFileName, uppercasedContent, "utf8", (err) => {
    if (err) {
      return callback(err, null);
      //writing the content to the new file
    }

    // Append the filename to "filenames.txt"
    fs.appendFile("filenames.txt", `${newFileName}\n`, (err) => {
      //adding the filename in filenames.txt file
      if (err) {
        return callback(err, null);
        //return callback if error occrued
      }
      callback(null, newFileName);
    });
  });
}

// Step 3: Read the new file, convert to lowercase, split into sentences & write to a new file
function convertToLowerCase(data, callback) {
  const lowercasedContent = data.toLowerCase();
  //converting to lowercase
  const sentences = lowercasedContent.split(/[.?]/);
  //splitting the content based upon fullstop or question mark

  const newFileName = "lowercased_split.txt";
  //the file where the lowercase letter will be stored

  fs.writeFile(newFileName, sentences.join("\n"), "utf8", (err) => {
    if (err) {
      return callback(err, null);
    }

    // Append the filename to "filenames.txt"
    fs.appendFile("filenames.txt", `${newFileName}\n`, (err) => {
      if (err) {
        return callback(err, null);
        //additing the filename to filenames.txt
      }
      callback(null, newFileName);
    });
  });
}

// Step 4: Read the new files, sort the content & write to a new file
function sortContent(files, callback) {
  const contentArray = [];

  function readAndCollect(index) {
    if (index < files.length) {
      fs.readFile(files[index], "utf8", (err, data) => {
        if (!err) {
          contentArray.push(data);
          //pushing the data in current array is error is not occured
        }
        readAndCollect(index + 1);
      });
    } else {
      const sortedContent = contentArray.sort().join("\n");
      const newFileName = "sorted_content.txt";
      //the sortedcontent and filename in which it has to be stored

      fs.writeFile(newFileName, sortedContent, "utf8", (err) => {
        if (err) {
          return callback(err, null);
        }

        // Append the filename to "filenames.txt"
        fs.appendFile("filenames.txt", `${newFileName}\n`, (err) => {
          if (err) {
            return callback(err, null);
          }
          callback(null, newFileName);
        });
      });
    }
  }

  readAndCollect(0);
  //initial value to pass to the index in readand collect function
}

// Step 5: Read contents of filenames.txt and delete all new files
function deleteFiles(filesToDelete, callback) {
  fs.readFile("filenames.txt", "utf8", (err, data) => {
    if (err) {
      return callback(err);
    }

    const filenames = data.split("\n");
    //stroing the values of filenames.txt

    filenames.forEach((filename) => {
      //deleting eact item from the filenames.txt
      fs.unlink(filename, (err) => {
        if (err) {
          console.error(err);
        } else {
          console.log(`${filename} deleted successfully`);
        }
      });
    });

    callback(null);
  });
}

module.exports = {
  readingFile,
  convertToUppercase,
  convertToLowerCase,
  sortContent,
  deleteFiles,
};
