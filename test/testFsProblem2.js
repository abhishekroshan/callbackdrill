const {
  readingFile,
  convertToUppercase,
  convertToLowerCase,
  sortContent,
  deleteFiles,
} = require("../fs-problem2");

const fileLocation = "../lipsum_1.txt";

readingFile(fileLocation, (err, data) => {
  if (err) {
    return console.log(err);
  }

  convertToUppercase(data, (err, uppercasedFile) => {
    if (err) {
      return console.log(err);
    }

    convertToLowerCase(data, (err, lowercasedSplitFile) => {
      if (err) {
        return console.log(err);
      }

      sortContent([uppercasedFile, lowercasedSplitFile], (err, sortedFile) => {
        if (err) {
          return console.log(err);
        }

        deleteFiles(
          [uppercasedFile, lowercasedSplitFile, sortedFile],
          (err) => {
            if (err) {
              return console.log(err);
            }

            console.log("Process completed successfully.");
          }
        );
      });
    });
  });
});
