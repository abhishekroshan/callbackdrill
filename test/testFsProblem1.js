const createAndDeleteFiles = require("../fs-problem1");

const directoryPath = "./randomJsonFiles"; // the directory path

createAndDeleteFiles(directoryPath, 5, (error) => {
  if (error) {
    console.error("Error:", error);
  }
});
